unless Vagrant.has_plugin?("vagrant-docker-compose")
  system "vagrant plugin install vagrant-docker-compose"
  exec "vagrant #{ARGV.join(' ')}"
  exit
end

Vagrant.configure("2") do |config|
  config.vm.box = "generic/ubuntu1804"

  config.vm.network :forwarded_port, guest: 2000, host: 2000
  config.vm.network :forwarded_port, guest: 10001, host: 10001

  # First two commands are a temporary workaround for Ubuntu boxes on VirtualBox on Windows.
  # There is a bug w.r.t. SHA256 hash checking when installing the docker provisioner as of late.
  # It seems to happen only on WSL-enabled systems, but this is better than asking the user to
  # disable WSL.
  config.vm.provision "shell",
    inline: <<SHELL
      mkdir /etc/gcrypt
      echo all >> /etc/gcrypt/hwf.deny
      mkdir /app
      cd /app
      wget https://gitlab.com/Riuga/wk3r-pi-local/-/raw/master/docker-compose.yml
      wget https://gitlab.com/Riuga/wk3r-pi-local/-/raw/master/init-db.sql
      touch packet.pck
SHELL

  pck = File.exist?("packet.pck") ? "/vagrant" : "/app"

  config.vm.provision :docker
  config.vm.provision :docker_compose, 
    env: { "PCK_ROOT" => "#{pck}" }, 
    yml: "/app/docker-compose.yml", 
    run: "always"
end
